package com.jrg.backendbookscase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.jrg.backendbookscase.file.property.FileStorageProperties;

@SpringBootApplication
@EnableConfigurationProperties({
    FileStorageProperties.class
})
public class BackendbookscaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendbookscaseApplication.class, args);
	}

}
