package com.jrg.backendbookscase.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * Entidad libro
 * 
 * @author Javier Rodríguez García
 *
 */
@Entity
@Table(name = "books")
public class Book {
	
	@Id
	@Column(name = "isbn")
	private long isbn;
	private String name;
	private int n_pag;
	private String summary;
	private String binding;
	private float price;
	private int stock;
    private String path_img;
	@ManyToOne(cascade=CascadeType.MERGE)
	@JoinColumn(name="id_languages")
    private Language language;
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name = "authors_books", joinColumns = {
			@JoinColumn(name = "id_isbn", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "id_authors", nullable = false, updatable = false) })
    private Set<Author> authors = new HashSet<Author>(0);
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name = "categories_books", joinColumns = {
			@JoinColumn(name = "id_isbn", nullable = false, updatable = false) }, inverseJoinColumns = {
			@JoinColumn(name = "id_categories", nullable = false, updatable = false) })
    private Set<Category> categories = new HashSet<Category>(0);
	
	public Book() {
	}

	public Book(long isbn, String name, int n_pag, String summary, String binding, float price, int stock,
			String path_img, Language language, Set<Author> authors, Set<Category> categories) {
		this.isbn = isbn;
		this.name = name;
		this.n_pag = n_pag;
		this.summary = summary;
		this.binding = binding;
		this.price = price;
		this.stock = stock;
		this.path_img = path_img;
		this.language = language;
		this.authors = authors;
		this.categories = categories;
	}

	public long getIsbn() {
		return isbn;
	}

	public void setIsbn(long isbn) {
		this.isbn = isbn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getN_pag() {
		return n_pag;
	}

	public void setN_pag(int n_pag) {
		this.n_pag = n_pag;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getBinding() {
		return binding;
	}

	public void setBinding(String binding) {
		this.binding = binding;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String getPath_img() {
		return path_img;
	}

	public void setPath_img(String path_img) {
		this.path_img = path_img;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public Set<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}

	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}
	


}
