package com.jrg.backendbookscase.entity;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Entidad autor
 * 
 * @author Javier Rodríguez García
 *
 */
@Entity
@Table(name = "authors")
public class Author {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private String surnames;

	
	public Author() {
	}

	public Author(String name, String surnames) {
		this.name = name;
		this.surnames = surnames;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurnames() {
		return surnames;
	}

	public void setSurnames(String surnames) {
		this.surnames = surnames;
	}


	

}
