package com.jrg.backendbookscase.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.jrg.backendbookscase.security.entity.User;



/**
 * Entidad venta
 * 
 * @author Javier Rodríguez García
 *
 */
@Entity
@Table(name = "sales")
public class Sales {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	@Column(name = "date", nullable = false)
	private Date salesDate;
	@ManyToOne(cascade=CascadeType.MERGE)
	@JoinColumn(name="id_users")
	private User user;
	
	public Sales() {
	}

	
	
	
	
	public Sales(Date salesDate, User user) {
		super();
		this.salesDate = salesDate;
		this.user = user;
	}





	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public Date getSalesDate() {
		return salesDate;
	}

	public void setSalesDate(Date salesDate) {
		this.salesDate = salesDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;

	}

	
	
	
	
}
