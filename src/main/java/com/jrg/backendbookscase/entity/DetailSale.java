package com.jrg.backendbookscase.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * Entidad detalle de la venta
 * 
 * @author Javier Rodríguez García
 *
 */
@Entity
@Table(name="detailsale")
public class DetailSale {
	
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name= "id", nullable=false)
	private int id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_sales", referencedColumnName = "id" , nullable = false, foreignKey = @ForeignKey(name = "fk_detailsale_sales1"))
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Sales sale;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_isbn", nullable = false, foreignKey = @ForeignKey(name = "fk_detailsale_books1"))
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Book book;
	@Column(name = "quantity", nullable = false)
	private int quantity;
	@Column(name = "price", nullable = false)
	private float price;
	
	public DetailSale() {

	}

	public DetailSale(Sales sale, Book book, int quantity, float price) {
		super();
		this.sale = sale;
		this.book = book;
		this.quantity = quantity;
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Sales getSale() {
		return sale;
	}

	public void setSale(Sales sale) {
		this.sale = sale;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	
	

}
