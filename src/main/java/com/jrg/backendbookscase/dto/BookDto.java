package com.jrg.backendbookscase.dto;

import java.util.HashSet;
import java.util.Set;


import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import com.jrg.backendbookscase.entity.Author;
import com.jrg.backendbookscase.entity.Category;
import com.jrg.backendbookscase.entity.Language;


/**
 * Clase dto libro
 * 
 * @author Javier Rodríguez García
 *
 */
public class BookDto {
	
	@NotBlank
	private long isbn;
	@NotBlank
	private String name;
	@Min(0)
	private int n_pag;
	@NotBlank
	private String summary;
	@NotBlank
	private String binding;
	@Min(0)
	private Float price;
	@Min(0)
	private int stock;
	@NotBlank
    private String path_img;
	@NotBlank
    private Language language;
	@NotBlank
    private Set<Author> authors = new HashSet<>();
	@NotBlank
    private Set<Category> categories = new HashSet<>();
	
	public BookDto() {
	}

	public BookDto(@NotBlank long isbn, @NotBlank String name, @NotBlank int n_pag, @NotBlank String summary,
			@NotBlank String binding, @Min(0) Float price, @NotBlank int stock, @NotBlank String path_img,
			@NotBlank Language language, @NotBlank Set<Author> authors, @NotBlank Set<Category> categories) {
		this.isbn = isbn;
		this.name = name;
		this.n_pag = n_pag;
		this.summary = summary;
		this.binding = binding;
		this.price = price;
		this.stock = stock;
		this.path_img = path_img;
		this.language = language;
		this.authors = authors;
		this.categories = categories;
	}

	public long getIsbn() {
		return isbn;
	}

	public void setIsbn(long isbn) {
		this.isbn = isbn;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getN_pag() {
		return n_pag;
	}

	public void setN_pag(int n_pag) {
		this.n_pag = n_pag;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getBinding() {
		return binding;
	}

	public void setBinding(String binding) {
		this.binding = binding;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String getPath_img() {
		return path_img;
	}

	public void setPath_img(String path_img) {
		this.path_img = path_img;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public Set<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}

	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}
}
