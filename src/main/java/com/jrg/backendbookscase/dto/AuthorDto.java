package com.jrg.backendbookscase.dto;

import javax.validation.constraints.NotBlank;

/**
 * Clase dto autor
 * 
 * @author Javier Rodríguez García
 *
 */
public class AuthorDto {
	
	@NotBlank
	private String name;
	@NotBlank
	private String surnames;


	public AuthorDto() {
	}

	public AuthorDto(@NotBlank String name, @NotBlank String surnames) {
		super();
		this.name = name;
		this.surnames = surnames;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurnames() {
		return surnames;
	}

	public void setSurnames(String surnames) {
		this.surnames = surnames;
	}
	
	
	
	

}
