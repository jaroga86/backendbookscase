package com.jrg.backendbookscase.dto;

import javax.validation.constraints.NotBlank;


/**
 * Clase dto lenguaje
 * 
 * @author Javier Rodríguez García
 *
 */
public class LanguageDto {
	
	@NotBlank
	private String language;

	public LanguageDto() {
	}

	public LanguageDto(@NotBlank String language) {
		this.language = language;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
	

}
