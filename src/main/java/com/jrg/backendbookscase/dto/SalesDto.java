package com.jrg.backendbookscase.dto;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import com.jrg.backendbookscase.security.entity.User;


/**
 * Clase dto Ventas
 * 
 * @author Javier Rodríguez García
 *
 */
public class SalesDto {
	
	@NotBlank
	private Date salesDate;
	@NotBlank
	private User user;
	
	
	
	public SalesDto() {
		super();
	}
	
	
	
	public SalesDto(@NotBlank Date salesDate, @NotBlank User user) {
		super();
		this.salesDate = salesDate;
		this.user = user;
	}

	public Date getSalesDate() {
		return salesDate;
	}
	public void setSalesDate(Date salesDate) {
		this.salesDate = salesDate;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	
}
