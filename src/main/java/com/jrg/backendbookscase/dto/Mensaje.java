package com.jrg.backendbookscase.dto;


/**
 * Clase dto mensaje. Esta clase se utiliza para guardar el mensajes que va mostrar en la respuestas.
 * 
 * @author Javier Rodríguez García
 *
 */
public class Mensaje {
    private String mensaje;

    public Mensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
