package com.jrg.backendbookscase.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import com.jrg.backendbookscase.entity.Book;
import com.jrg.backendbookscase.entity.Sales;

/**
 * Clase dto detalle de la venta
 * 
 * @author Javier Rodríguez García
 *
 */
public class DetailSaleDto {
	
	@NotBlank
	private Sales sale;
	@NotBlank
	private Book book;
	@Min(0)
	private int quantity;
	@Min(0)
	private float price;
	
	public DetailSaleDto() {
	}

	public DetailSaleDto(@NotBlank Sales sale, @NotBlank Book book, @Min(0) int quantity, @Min(0) float price) {
		super();
		this.sale = sale;
		this.book = book;
		this.quantity = quantity;
		this.price = price;
	}

	public Sales getSale() {
		return sale;
	}

	public void setSale(Sales sale) {
		this.sale = sale;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
}
