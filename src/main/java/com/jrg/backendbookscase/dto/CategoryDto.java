package com.jrg.backendbookscase.dto;

import javax.validation.constraints.NotBlank;

/**
 * Clase dto categoria
 * 
 * @author Javier Rodríguez García
 *
 */
public class CategoryDto {
	
	@NotBlank
	private String name;

	public CategoryDto() {
	}

	public CategoryDto(@NotBlank String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
