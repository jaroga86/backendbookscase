package com.jrg.backendbookscase.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.jrg.backendbookscase.entity.Category;

/**
 * Repository categoria, interfaz para realizar las queries en base de datos extiende de la clase JpaRepository
 * en la que vienen ya las queries prediseñadas.
 * 
 * @author Javier Rodríguez García
 *
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer>{
	
	Optional<Category> findByName(String name);
	boolean existsByName(String name);

}
