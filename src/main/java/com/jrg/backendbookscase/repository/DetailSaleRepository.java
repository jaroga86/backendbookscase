package com.jrg.backendbookscase.repository;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jrg.backendbookscase.entity.DetailSale;
import com.jrg.backendbookscase.entity.Sales;

/**
 * Repository detalle de la venta, interfaz para realizar las queries en base de datos extiende de la clase JpaRepository
 * en la que vienen ya las queries prediseñadas.
 * 
 * @author Javier Rodríguez García
 *
 */
@Repository
public interface DetailSaleRepository extends JpaRepository<DetailSale, Integer> {
	
	List<DetailSale> findBySale(Sales sale);
	
	

}
