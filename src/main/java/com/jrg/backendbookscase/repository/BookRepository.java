package com.jrg.backendbookscase.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;


import com.jrg.backendbookscase.entity.Book;


/**
 * Repository libros, interfaz para realizar las queries en base de datos extiende de la clase JpaRepository
 * en la que vienen ya las queries prediseñadas.
 * 
 * @author Javier Rodríguez García
 *
 */
@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
	


}
