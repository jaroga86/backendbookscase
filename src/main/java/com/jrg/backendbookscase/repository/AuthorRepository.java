package com.jrg.backendbookscase.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jrg.backendbookscase.entity.Author;

/**
 * Repository autor, interfaz para realizar las queries en base de datos extiende de la clase JpaRepository
 * en la que vienen ya las queries prediseñadas.
 * 
 * @author Javier Rodríguez García
 *
 */
@Repository
public interface AuthorRepository extends JpaRepository<Author, Integer> {
	
	Optional<Author> findByNameAndSurnames(String name, String surnames);
	boolean existsByNameAndSurnames(String name, String surnames);
	

}
