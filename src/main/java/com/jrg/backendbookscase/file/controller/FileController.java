package com.jrg.backendbookscase.file.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.jrg.backendbookscase.dto.Mensaje;
import com.jrg.backendbookscase.file.service.FileService;

import io.swagger.annotations.ApiOperation;

/**
 * Controlador de la entidad file. Este se ha creado para subir las imagenes a un servidor.
 * 
 * @author Javier Rodríguez García
 *
 */
@RestController
@RequestMapping("/file")
public class FileController {

	private static final Logger logger = LoggerFactory.getLogger(FileController.class);

	@Autowired
	private FileService fileService;
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Sube una image al servidor, es necesario tener permisos de administrador")
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/uploadFile")
	public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
		String fileName = fileService.storeFile(file);

		return new ResponseEntity(new Mensaje("La imagen " + fileName + " se ha subido correctamente."), HttpStatus.OK);
	}


	@ApiOperation("Descarga una image del servidor.")
	@GetMapping("/portadas/{fileName:.+}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
		Resource resource = fileService.loadFileAsResource(fileName);

		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (IOException ex) {
			logger.info("No puede determinar el tipo de fichero");
		}

		if (contentType == null) {
			contentType = "application/octet-stream";
		}

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}

}
