package com.jrg.backendbookscase.security.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jrg.backendbookscase.security.entity.Rol;
import com.jrg.backendbookscase.security.enums.RolName;


/**
 * Repository Rol, interfaz para realizar las queries en base de datos extiende de la clase JpaRepository
 * en la que vienen ya las queries prediseñadas.
 * 
 * @author Javier Rodríguez García
 *
 */
@Repository
public interface RoleRepository extends JpaRepository<Rol, Integer> {
	
	Optional<Rol> findByRolName(RolName rolName);
}
