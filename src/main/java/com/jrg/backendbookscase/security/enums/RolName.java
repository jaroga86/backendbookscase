package com.jrg.backendbookscase.security.enums;


/**
 * Enumerado tipos de Roles
 * 
 * @author Javier Rodríguez García
 *
 */
public enum RolName {
    ROLE_ADMIN, ROLE_USER
}
