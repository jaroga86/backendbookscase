package com.jrg.backendbookscase.security.controller;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jrg.backendbookscase.dto.Mensaje;
import com.jrg.backendbookscase.security.dto.JwtDto;
import com.jrg.backendbookscase.security.dto.LoginUser;
import com.jrg.backendbookscase.security.dto.NewUser;
import com.jrg.backendbookscase.security.entity.Rol;
import com.jrg.backendbookscase.security.entity.User;
import com.jrg.backendbookscase.security.enums.RolName;
import com.jrg.backendbookscase.security.jwt.JwtProvider;
import com.jrg.backendbookscase.security.service.RolService;
import com.jrg.backendbookscase.security.service.UserService;

import io.swagger.annotations.ApiOperation;



@RestController
@RequestMapping("/auth")
@CrossOrigin
public class AuthController {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserService userService;

    @Autowired
    RolService rolService;

    @Autowired
    JwtProvider jwtProvider;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Crea un nuevo usuario")
    @PostMapping("/nuevo")
    public ResponseEntity<?> nuevo(@Valid @RequestBody NewUser newUser, BindingResult bindingResult){
		if(StringUtils.isBlank(newUser.getName()))
            return new ResponseEntity(new Mensaje("el nombre es obligatorio"), HttpStatus.BAD_REQUEST);
		if(StringUtils.isBlank(newUser.getSurnames()))
            return new ResponseEntity(new Mensaje("los apellidos son obligatorio"), HttpStatus.BAD_REQUEST);
		if(newUser.getCp() == 0 )
            return new ResponseEntity(new Mensaje("el codigo postal no puede estar vacio"), HttpStatus.BAD_REQUEST);
		if(StringUtils.isBlank(newUser.getLocation()))
            return new ResponseEntity(new Mensaje("la ciudad es obligatoria"), HttpStatus.BAD_REQUEST);
		if(StringUtils.isBlank(newUser.getProvince()))
            return new ResponseEntity(new Mensaje("la provincia es obligatoria"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(newUser.getPassword()))
            return new ResponseEntity(new Mensaje("la contraseña es obligatoria"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(newUser.getEmail()))
            return new ResponseEntity(new Mensaje("el email es obligatorio"), HttpStatus.BAD_REQUEST);
        if(bindingResult.hasErrors())
            return new ResponseEntity(new Mensaje("campos mal puestos o email inválido"), HttpStatus.BAD_REQUEST);
        if(userService.existsByEmail(newUser.getEmail()))
            return new ResponseEntity(new Mensaje("ese email ya existe"), HttpStatus.BAD_REQUEST);
        User user = new User(newUser.getName(), newUser.getSurnames(), passwordEncoder.encode(newUser.getPassword()), newUser.getEmail(), newUser.getAddress(), newUser.getCp(), newUser.getLocation(), newUser.getProvince(), newUser.isState());

        Set<Rol> roles = new HashSet<>();
        roles.add(rolService.getByRolName(RolName.ROLE_USER).get());
        if(newUser.getRoles().contains("admin"))
            roles.add(rolService.getByRolName(RolName.ROLE_ADMIN).get());
        user.setRoles(roles);
        userService.save(user);
        return new ResponseEntity(new Mensaje("usuario creado"), HttpStatus.CREATED);
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Permite logarse en la aplicación")
    @PostMapping("/login")
    public ResponseEntity<JwtDto> login(@Valid @RequestBody LoginUser loginUser, BindingResult bindingResult){
        if(bindingResult.hasErrors())
            return new ResponseEntity(new Mensaje("campos mal puestos"), HttpStatus.BAD_REQUEST);
        Authentication authentication =
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginUser.getEmail(), loginUser.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtProvider.generateToken(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        
        Optional<User> user = userService.getByEmail(userDetails.getUsername());
        JwtDto jwtDto = new JwtDto(jwt, user.get(), userDetails.getAuthorities());
        return new ResponseEntity(jwtDto, HttpStatus.OK);
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Refresca el token y obtiene uno nuevo")
    @PostMapping("/refresh")
    public ResponseEntity<JwtDto> refresh(@RequestBody JwtDto jwtDto) throws ParseException {
        String token = jwtProvider.refreshToken(jwtDto);
        JwtDto jwt = new JwtDto(token);
        return new ResponseEntity(jwt, HttpStatus.OK);
    }
}
