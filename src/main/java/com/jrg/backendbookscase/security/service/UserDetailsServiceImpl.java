package com.jrg.backendbookscase.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.jrg.backendbookscase.security.entity.User;
import com.jrg.backendbookscase.security.entity.UserMain;



/**
 * Capa de servicio detalle del usuario donde se implementa la interfaz.
 * 
 * @author Javier Rodríguez García
 *
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	UserService userService;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		User user = userService.getByEmail(email).get();
		
		return UserMain.build(user);
	}
	
	
}
