package com.jrg.backendbookscase.security.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jrg.backendbookscase.security.entity.Rol;
import com.jrg.backendbookscase.security.enums.RolName;
import com.jrg.backendbookscase.security.repository.RoleRepository;

/**
 * Capa de servicio del rol donde se implementa la interfaz.
 * 
 * @author Javier Rodríguez García
 *
 */
@Service
@Transactional
public class RolService {
	
	@Autowired
	RoleRepository rolRepository;
	
	public Optional<Rol> getByRolName(RolName rolName){
		return rolRepository.findByRolName(rolName);
	}
}
