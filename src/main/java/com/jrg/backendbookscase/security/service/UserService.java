package com.jrg.backendbookscase.security.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jrg.backendbookscase.security.entity.User;
import com.jrg.backendbookscase.security.repository.UserRepository;




/**
 * Capa de servicio del usuario donde se implementa la interfaz.
 * 
 * @author Javier Rodríguez García
 *
 */
@Service
@Transactional
public class UserService {
	
	@Autowired
	UserRepository userRepository;
	
	public Optional<User> getByEmail(String email){
		return userRepository.findByEmail(email);
	}
	
	
	public boolean existsByEmail(String email) {
		return userRepository.existsByEmail(email);
	}
	
	public void save(User user) {
		userRepository.save(user);
	}
}
