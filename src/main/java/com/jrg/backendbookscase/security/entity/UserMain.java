package com.jrg.backendbookscase.security.entity;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Entidad Usuario principal que implemnta detalle del usuario.
 * 
 * @author Javier Rodríguez García
 *
 */
public class UserMain implements UserDetails {
	
    private String name;
    private String surnames;
    private String password;
    private String email;
    private String address;
    private int cp;
    private String location;
    private String province;
    private boolean state;
    private Collection<? extends GrantedAuthority> authorities;
    
    
    
    
	public UserMain(String name, String surnames, String password, String email, String address, int cp,
			String location, String province, boolean state, Collection<? extends GrantedAuthority> authorities) {
		super();
		this.name = name;
		this.surnames = surnames;
		this.password = password;
		this.email = email;
		this.address = address;
		this.cp = cp;
		this.location = location;
		this.province = province;
		this.state = state;
		this.authorities = authorities;
	}
	
	public static UserMain build(User user) {
		List<GrantedAuthority> authorities =
				user.getRoles().stream().map(rol -> new SimpleGrantedAuthority(rol.getRolName().name())).collect(Collectors.toList());
		return new UserMain(user.getName(), user.getSurnames(), user.getPassword(), user.getEmail(), user.getAddress(), user.getCp(), user.getLocation(), user.getProvince(), user.isState(), authorities);
	}
	
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}
	@Override
	public String getPassword() {
		return password;
	}
	@Override
	public String getUsername() {
		return email;
	}
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	@Override
	public boolean isEnabled() {
		return true;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSurnames() {
		return surnames;
	}


	public void setSurnames(String surnames) {
		this.surnames = surnames;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public int getCp() {
		return cp;
	}


	public void setCp(int cp) {
		this.cp = cp;
	}


	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public String getProvince() {
		return province;
	}


	public void setProvince(String province) {
		this.province = province;
	}


	public boolean isState() {
		return state;
	}


	public void setState(boolean state) {
		this.state = state;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}
	
	
	
	
}
