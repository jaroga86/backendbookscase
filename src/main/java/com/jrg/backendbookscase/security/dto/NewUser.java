package com.jrg.backendbookscase.security.dto;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import com.jrg.backendbookscase.security.entity.Rol;



/**
 * Clase Nuevo usuario que será necesaria para crear un usuario.
 * 
 * @author Javier Rodríguez García
 *
 */
public class NewUser {
	
    @NotBlank
    private String name;
    @NotBlank
    private String surnames;
    @NotBlank
    private String password;
    @Email
    private String email;
    @NotBlank
    private String address;
    @NotBlank
    private int cp;
    @NotBlank
    private String location;
    @NotBlank
    private String province;
    @NotBlank
    private boolean state;
    private Set<String> roles = new HashSet<>();
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurnames() {
		return surnames;
	}
	public void setSurnames(String surnames) {
		this.surnames = surnames;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getCp() {
		return cp;
	}
	public void setCp(int cp) {
		this.cp = cp;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public boolean isState() {
		return state;
	}
	public void setState(boolean state) {
		this.state = state;
	}
	public Set<String> getRoles() {
		return roles;
	}
	public void setRoles(Set<String> roles) {
		this.roles = roles;
	}
    
    
}
