package com.jrg.backendbookscase.security.dto;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import com.jrg.backendbookscase.security.entity.User;

/**
 * Clase que dto que almacenará el token y el rol del usuario.
 * 
 * @author Javier Rodríguez García
 *
 */
public class JwtDto {
    private String token;
    private String bearer = "Bearer";
    private User user;
    private Collection<? extends GrantedAuthority> authorities;

    
    public JwtDto(String token, User user, Collection<? extends GrantedAuthority> authorities) {
        this.token = token;
        this.user = user;
        this.authorities = authorities;
    }
    

    public JwtDto(String token) {
		super();
		this.token = token;
	}



	public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBearer() {
        return bearer;
    }

    public void setBearer(String bearer) {
        this.bearer = bearer;
    }



    public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }
}
