package com.jrg.backendbookscase.security.dto;

import javax.validation.constraints.NotBlank;


/**
 * Clase Login Dto que será necesaria para logarse.
 * 
 * @author Javier Rodríguez García
 *
 */
public class LoginUser {
    @NotBlank
    private String email;
    @NotBlank
    private String password;



    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
