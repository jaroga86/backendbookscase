package com.jrg.backendbookscase.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jrg.backendbookscase.repository.AuthorRepository;
import com.jrg.backendbookscase.entity.Author;



/**
 * Capa de servicio autor donde se implementa la interfaz.
 * 
 * @author Javier Rodríguez García
 *
 */
@Service
@Transactional
public class AuthorService {
	
	@Autowired
	AuthorRepository authorRepository;
	
	
	public List<Author> list(){
		return authorRepository.findAll();
	}
	
	public Optional<Author> getOne(int id){
		return authorRepository.findById(id);	
	}
	
	public Optional<Author> getByNombreAndSurnames(String name,String surnames){
		return authorRepository.findByNameAndSurnames(name, surnames);
	}
	
	public void save(Author author) {
		authorRepository.save(author);
	}
	
	public void delete(int id) {
		authorRepository.deleteById(id);
	}
	
	public boolean existsById(int id) {
		return authorRepository.existsById(id);
	}
	
	public boolean existsByNameAndSurnames(String name, String surnames) {
		return authorRepository.existsByNameAndSurnames(name, surnames);
	}

}
