package com.jrg.backendbookscase.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.jrg.backendbookscase.entity.DetailSale;
import com.jrg.backendbookscase.entity.Sales;
import com.jrg.backendbookscase.repository.DetailSaleRepository;


/**
 * Capa de servicio detalle de la venta donde se implementa la interfaz.
 * 
 * @author Javier Rodríguez García
 *
 */
@Service
@Transactional
public class DetailSaleService {
	
	@Autowired
	DetailSaleRepository detailSaleRepository;
	
	public List<DetailSale> list(){
		return detailSaleRepository.findAll();
	}
	
	public Optional<DetailSale> getOne(int id){
		
		
		return detailSaleRepository.findById(id);	
	}
	
	public List<DetailSale> findByIdSale(Sales sale){
		
		return detailSaleRepository.findBySale(sale);
	}
	
	
	public void save(DetailSale detailSale) {
		detailSaleRepository.save(detailSale);
	}
	
	public void delete(int id) {
		detailSaleRepository.deleteById(id);
	}
	
	public boolean existsById(int id) {
		return detailSaleRepository.existsById(id);
	}



}
