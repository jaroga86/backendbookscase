package com.jrg.backendbookscase.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.jrg.backendbookscase.entity.Book;
import com.jrg.backendbookscase.repository.BookRepository;


/**
 * Capa de servicio libro donde se implementa la interfaz.
 * 
 * @author Javier Rodríguez García
 *
 */
@Service
@Transactional
public class BookService {
	
	@Autowired
	BookRepository bookRepository;
	
	
	public List<Book> list(){
		return bookRepository.findAll();
	}
	
	public Optional<Book> getOne(long isbn){
		return bookRepository.findById(isbn);	
	}
	
/*	public Optional<Book> getByISBN(long isbn){
		return bookRepository.findByISBN(isbn);
	}*/
	
	public void save(Book book) {
		bookRepository.save(book);
	}
	
	public void delete(long isbn) {
		bookRepository.deleteById(isbn);
	}
	
	
	public boolean existsById(long isbn) {
		return bookRepository.existsById(isbn);
	}
	
	/*public boolean existsByISBN(long isbn) {
		return bookRepository.existsByISBN(isbn);
	}*/

}
