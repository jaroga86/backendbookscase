package com.jrg.backendbookscase.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jrg.backendbookscase.entity.Sales;
import com.jrg.backendbookscase.repository.SalesRepository;


/**
 * Capa de servicio Venta donde se implementa la interfaz.
 * 
 * @author Javier Rodríguez García
 *
 */
@Service
@Transactional
public class SalesService {
	
	@Autowired
	SalesRepository salesRepository;
	
	
	public List<Sales> list(){
		return salesRepository.findAll();
	}
	
	public Optional<Sales> getOne(int id){
		return salesRepository.findById(id);	
	}
	
	public Sales save(Sales sales) {
		return salesRepository.save(sales);
	}
	
	public void delete(int id) {
		salesRepository.deleteById(id);
	}
	
	public boolean existsById(int id) {
		return salesRepository.existsById(id);
	}
	


}
