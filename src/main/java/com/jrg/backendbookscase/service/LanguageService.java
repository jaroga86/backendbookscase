package com.jrg.backendbookscase.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jrg.backendbookscase.entity.Language;
import com.jrg.backendbookscase.repository.LanguageRepository;



/**
 * Capa de servicio lenguaje donde se implementa la interfaz.
 * 
 * @author Javier Rodríguez García
 *
 */
@Service
@Transactional
public class LanguageService {
	
	@Autowired
	LanguageRepository languageRepository;
	
	
	public List<Language> list(){
		return languageRepository.findAll();
	}
	
	public Optional<Language> getOne(int id){
		return languageRepository.findById(id);	
	}
	
	public Optional<Language> getByLanguage(String language){
		return languageRepository.findByLanguage(language);
	}
	
	public void save(Language language) {
		languageRepository.save(language);
	}
	
	public void delete(int id) {
		languageRepository.deleteById(id);
	}
	
	public boolean existsById(int id) {
		return languageRepository.existsById(id);
	}
	
	public boolean existsByLanguage(String language) {
		return languageRepository.existsByLanguage(language);
	}


}
