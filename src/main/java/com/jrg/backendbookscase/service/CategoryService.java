package com.jrg.backendbookscase.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jrg.backendbookscase.entity.Category;
import com.jrg.backendbookscase.repository.CategoryRepository;


/**
 * Capa de servicio categoria donde se implementa la interfaz.
 * 
 * @author Javier Rodríguez García
 *
 */
@Service
@Transactional
public class CategoryService {
	
	@Autowired
	CategoryRepository categoryRepository;
	
	
	public List<Category> list(){
		return categoryRepository.findAll();
	}
	
	public Optional<Category> getOne(int id){
		return categoryRepository.findById(id);	
	}
	
	public Optional<Category> getByName(String name){
		return categoryRepository.findByName(name);
	}
	
	public void save(Category category) {
		categoryRepository.save(category);
	}
	
	public void delete(int id) {
		categoryRepository.deleteById(id);
	}
	
	public boolean existsById(int id) {
		return categoryRepository.existsById(id);
	}
	
	public boolean existsByName(String name) {
		return categoryRepository.existsByName(name);
	}

}
