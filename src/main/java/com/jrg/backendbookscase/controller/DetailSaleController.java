package com.jrg.backendbookscase.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jrg.backendbookscase.dto.DetailSaleDto;
import com.jrg.backendbookscase.dto.Mensaje;
import com.jrg.backendbookscase.entity.DetailSale;
import com.jrg.backendbookscase.entity.Sales;
import com.jrg.backendbookscase.service.DetailSaleService;
import com.jrg.backendbookscase.service.SalesService;

import io.swagger.annotations.ApiOperation;


/**
 * Controlador de la entidad detalle de la venta.
 * 
 * @author Javier Rodríguez García
 *
 */
@RestController
@RequestMapping("/detailsale")
@CrossOrigin(origins = "*")
public class DetailSaleController {
	
	@Autowired
	DetailSaleService detailSaleService;
	
	@Autowired
	SalesService saleService;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Muestra una lista de detalles de la venta")
	@GetMapping("/lista")
	public ResponseEntity<List<DetailSale>> list(){
		List<DetailSale> list = detailSaleService.list();
		return new ResponseEntity(list, HttpStatus.OK);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Muestra el detalle de la venta por id")
    @GetMapping("/detail/{id}")
    public ResponseEntity<DetailSale> getById(@PathVariable("id") int id){
        if(!detailSaleService.existsById(id))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        DetailSale detailSale = detailSaleService.getOne(id).get();
        return new ResponseEntity(detailSale, HttpStatus.OK);
    }
    
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Muestra el detalle de la venta por el id de la venta")
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/bySale/{idSale}")
    public ResponseEntity<DetailSale> getBySaleId(@PathVariable("idSale") int idSale){
    	if(!saleService.existsById(idSale))
    		 return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
    	
    	Sales sales = saleService.getOne(idSale).get();
    	
    	List<DetailSale> listDetailSale = detailSaleService.findByIdSale(sales);
    	
    	
    	
    	return new ResponseEntity(listDetailSale, HttpStatus.OK);
    }
    
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Crea el detalle de la venta, lo puden crear sólo el admistrador y el cliente")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody DetailSaleDto detailSaleDto){
    	
    	
    	DetailSale detailSale = new DetailSale();
    	
    	detailSale.setBook(detailSaleDto.getBook());
    	detailSale.setSale(detailSaleDto.getSale());
    	detailSale.setQuantity(detailSaleDto.getQuantity());
    	detailSale.setPrice(detailSaleDto.getPrice());
    	
    	
    	
    	detailSaleService.save(detailSale);
    	
    	
    	
		return new ResponseEntity(new Mensaje("detalle de la venta creada"), HttpStatus.OK);
    	
    }
    
  
    
    
}
