package com.jrg.backendbookscase.controller;

import java.util.List;


import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jrg.backendbookscase.dto.AuthorDto;
import com.jrg.backendbookscase.dto.Mensaje;
import com.jrg.backendbookscase.entity.Author;
import com.jrg.backendbookscase.service.AuthorService;


import io.swagger.annotations.ApiOperation;


/**
 * Controlador de la entidad Autor.
 * 
 * @author Javier Rodríguez García
 *
 */
@RestController
@RequestMapping("/author")
@CrossOrigin(origins = "*")
public class AuthorController {
	
	@Autowired
	AuthorService authorService;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Muestra una lista de autores")
	@GetMapping("/lista")
	public ResponseEntity<List<Author>> list(){
		List<Author> list = authorService.list();
		return new ResponseEntity(list, HttpStatus.OK);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Muestra un autor pasandole el identificador")
    @GetMapping("/detail/{id}")
    public ResponseEntity<Author> getById(@PathVariable("id") int id){
        if(!authorService.existsById(id))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        Author author = authorService.getOne(id).get();
        return new ResponseEntity(author, HttpStatus.OK);
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Muestra el detalle de un autor pasandole el nombre y el apellido")
    @GetMapping("/detailnamesurnames/{name}/{surnames}")
    public ResponseEntity<Author> getNameSurnames(@PathVariable("name") String name, @PathVariable("surnames") String surnames){
        if(!authorService.existsByNameAndSurnames(name, surnames))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        Author author = authorService.getByNombreAndSurnames(name, surnames).get();
        return new ResponseEntity(author, HttpStatus.OK);
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Crea un Autor, sólo lo puede crear el administrador")
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody AuthorDto authorDto){
        if(StringUtils.isBlank(authorDto.getName()))
            return new ResponseEntity(new Mensaje("el nombre es obligatorio"), HttpStatus.BAD_REQUEST);
        if(authorService.existsByNameAndSurnames(authorDto.getName(), authorDto.getSurnames()))
            return new ResponseEntity(new Mensaje("ese autor ya existe"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(authorDto.getSurnames()))
            return new ResponseEntity(new Mensaje("los apellidos son obligatorio"), HttpStatus.BAD_REQUEST);
        Author author = new Author(authorDto.getName(), authorDto.getSurnames());
        authorService.save(author);
        return new ResponseEntity(new Mensaje("autor creado"), HttpStatus.OK);
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Actualiza un Autor a través del identificador")
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable("id")int id, @RequestBody AuthorDto authorDto){
        if(!authorService.existsById(id))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        if(authorService.existsByNameAndSurnames(authorDto.getName(), authorDto.getSurnames()) && authorService.getByNombreAndSurnames(authorDto.getName(), authorDto.getSurnames()).get().getId() != id)
            return new ResponseEntity(new Mensaje("ese autor ya existe"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(authorDto.getName()))
            return new ResponseEntity(new Mensaje("el nombre es obligatorio"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(authorDto.getSurnames()))
            return new ResponseEntity(new Mensaje("los apellidos son obligatorio"), HttpStatus.BAD_REQUEST);

        Author author = authorService.getOne(id).get();
        author.setName(authorDto.getName());
        author.setSurnames(authorDto.getSurnames());
        authorService.save(author);
        return new ResponseEntity(new Mensaje("autor actualizado"), HttpStatus.OK);
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Elimina un autor a través del identificador")
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id")int id){
        if(!authorService.existsById(id))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        try {
        	authorService.delete(id);
        }catch(Exception ex) {
        	return new ResponseEntity(new Mensaje("el autor no se puede eliminar porque está en uso"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(new Mensaje("autor eliminado"), HttpStatus.OK);
    }
	

}
