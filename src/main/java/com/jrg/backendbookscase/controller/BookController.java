package com.jrg.backendbookscase.controller;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.jrg.backendbookscase.dto.BookDto;
import com.jrg.backendbookscase.dto.Mensaje;
import com.jrg.backendbookscase.entity.Author;
import com.jrg.backendbookscase.entity.Book;
import com.jrg.backendbookscase.entity.Category;
import com.jrg.backendbookscase.entity.Language;
import com.jrg.backendbookscase.service.AuthorService;
import com.jrg.backendbookscase.service.BookService;
import com.jrg.backendbookscase.service.CategoryService;
import com.jrg.backendbookscase.service.LanguageService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/book")
@CrossOrigin(origins = "*")
public class BookController {
	
	@Autowired
	BookService bookService;
	
	@Autowired
	LanguageService languageService;
	
	@Autowired
	AuthorService authorService;
	
	@Autowired
	CategoryService categoryService;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Muestra una lista de libros")
	@GetMapping("/lista")
	public ResponseEntity<List<Book>> list(){
		List<Book> list = bookService.list();
		return new ResponseEntity(list, HttpStatus.OK);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Muestra un libro por ISBN")
    @GetMapping("/detail/{isbn}")
    public ResponseEntity<Book> getById(@PathVariable("isbn") long isbn){
        if(!bookService.existsById(isbn))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        Book book = bookService.getOne(isbn).get();
        return new ResponseEntity(book, HttpStatus.OK);
    }
    
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Crea un libro, necesario permisos de administrador")
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody BookDto bookDto){
        if(bookDto.getIsbn() == 0)
            return new ResponseEntity(new Mensaje("el isbn es obligatorio tiene que ser de 13 números"), HttpStatus.BAD_REQUEST);
        if(String.valueOf(bookDto.getIsbn()).length() < 13)
            return new ResponseEntity(new Mensaje("la longitud del isbn tiene que ser de 13 números"), HttpStatus.BAD_REQUEST);

        if(bookService.existsById(bookDto.getIsbn()))
            return new ResponseEntity(new Mensaje("el isbn ya exite"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(bookDto.getName()))
            return new ResponseEntity(new Mensaje("el nombre es obligatorio"), HttpStatus.BAD_REQUEST);
        if(Integer.toString(bookDto.getN_pag()) ==null || bookDto.getN_pag() <0)
            return new ResponseEntity(new Mensaje("el número de páginas es obligatorio"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(bookDto.getSummary()))
            return new ResponseEntity(new Mensaje("el resumen es obligatorio"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(bookDto.getBinding()))
            return new ResponseEntity(new Mensaje("el tipo es obligatorio"), HttpStatus.BAD_REQUEST);
        if(bookDto.getPrice()==null || bookDto.getPrice()<0)
            return new ResponseEntity(new Mensaje("el precio debe ser mayor que 0"), HttpStatus.BAD_REQUEST);
        if(Integer.toString(bookDto.getStock()) ==null || bookDto.getStock() <= 0)
            return new ResponseEntity(new Mensaje("el stock debe ser mayor o igual 0"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(bookDto.getPath_img()))
            return new ResponseEntity(new Mensaje("el path es obligatorio"), HttpStatus.BAD_REQUEST);
        if(bookDto.getLanguage() == null)
        	return new ResponseEntity(new Mensaje("añadir un lenguaje"), HttpStatus.BAD_REQUEST);
        if(!languageService.existsByLanguage(bookDto.getLanguage().getLanguage()))
        	return new ResponseEntity(new Mensaje("el lenguaje no existe"), HttpStatus.BAD_REQUEST);
        if(bookDto.getAuthors() == null || bookDto.getAuthors().size() == 0)
        	return new ResponseEntity(new Mensaje("añadir un autor"), HttpStatus.BAD_REQUEST);
        
        Iterator<?> authors = bookDto.getAuthors().iterator();
       
   while (authors.hasNext()) {
       Author author = (Author) authors.next();
       if(!authorService.existsByNameAndSurnames(author.getName(), author.getSurnames()))
       	return new ResponseEntity(new Mensaje("el autor no existe"), HttpStatus.BAD_REQUEST);
   }
       
       	if(bookDto.getCategories() == null || bookDto.getCategories().size() == 0)
       	return new ResponseEntity(new Mensaje("añadir una categoria"), HttpStatus.BAD_REQUEST);
       
	      Iterator<?> categories = bookDto.getCategories().iterator();
	      
	      while (categories.hasNext()) {
	          Category category = (Category) categories.next();
	          if(!categoryService.existsByName(category.getName()))
	          	return new ResponseEntity(new Mensaje("la categoria no existe"), HttpStatus.BAD_REQUEST);
	      }
       
        
        Language language = languageService.getByLanguage(bookDto.getLanguage().getLanguage()).get();
        Book book = new Book(bookDto.getIsbn(), bookDto.getName(), bookDto.getN_pag(), bookDto.getSummary(), bookDto.getBinding(), bookDto.getPrice(), bookDto.getStock(), bookDto.getPath_img(), bookDto.getLanguage(), bookDto.getAuthors(), bookDto.getCategories());
        bookService.save(book);
        return new ResponseEntity(new Mensaje("book creado"), HttpStatus.OK);
    }
    
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Actualiza un libro por isbn, necesario permisos de administrador")
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/update/{isbn}")
    public ResponseEntity<?> update(@PathVariable("isbn")long isbn, @RequestBody BookDto bookDto){
        if(!bookService.existsById(isbn))
            return new ResponseEntity(new Mensaje("el isbn no existe"), HttpStatus.NOT_FOUND);
        if(StringUtils.isBlank(bookDto.getName()))
            return new ResponseEntity(new Mensaje("el nombre es obligatorio"), HttpStatus.BAD_REQUEST);
        if(Integer.toString(bookDto.getN_pag()) ==null || bookDto.getN_pag() <0)
            return new ResponseEntity(new Mensaje("el número de páginas es obligatorio"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(bookDto.getSummary()))
            return new ResponseEntity(new Mensaje("el resumen es obligatorio"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(bookDto.getBinding()))
            return new ResponseEntity(new Mensaje("el tipo es obligatorio"), HttpStatus.BAD_REQUEST);
        if(bookDto.getPrice()==null || bookDto.getPrice()<0)
            return new ResponseEntity(new Mensaje("el precio debe ser mayor que 0"), HttpStatus.BAD_REQUEST);
        if(Integer.toString(bookDto.getStock()) ==null || bookDto.getStock() <= 0)
            return new ResponseEntity(new Mensaje("el stock debe ser mayor o igual 0"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(bookDto.getPath_img()))
            return new ResponseEntity(new Mensaje("el path es obligatorio"), HttpStatus.BAD_REQUEST);
        if(bookDto.getLanguage() == null)
        	return new ResponseEntity(new Mensaje("añadir un lenguaje"), HttpStatus.BAD_REQUEST);
        if(!languageService.existsByLanguage(bookDto.getLanguage().getLanguage()))
        	return new ResponseEntity(new Mensaje("el lenguaje no existe"), HttpStatus.BAD_REQUEST);
        if(bookDto.getAuthors() == null || bookDto.getAuthors().size() == 0)
        	return new ResponseEntity(new Mensaje("añadir un autor"), HttpStatus.BAD_REQUEST);

        Iterator<?> authors = bookDto.getAuthors().iterator();
        
		   while (authors.hasNext()) {
		       Author author = (Author) authors.next();
		       if(!authorService.existsByNameAndSurnames(author.getName(), author.getSurnames()))
		       	return new ResponseEntity(new Mensaje("el autor no existe"), HttpStatus.BAD_REQUEST);
		   }
       
       	if(bookDto.getCategories() == null || bookDto.getCategories().size() == 0)
       	return new ResponseEntity(new Mensaje("añadir una categoria"), HttpStatus.BAD_REQUEST);
       
	      Iterator<?> categories = bookDto.getCategories().iterator();
	      
	      while (categories.hasNext()) {
	          Category category = (Category) categories.next();
	          if(!categoryService.existsByName(category.getName()))
	          	return new ResponseEntity(new Mensaje("la categoria no existe"), HttpStatus.BAD_REQUEST);
	      }
       
        Book book = bookService.getOne(isbn).get();
        book.setName(bookDto.getName());
        book.setN_pag(bookDto.getN_pag());
        book.setSummary(bookDto.getSummary());
        book.setBinding(bookDto.getBinding());
        book.setPrice(bookDto.getPrice());
        book.setStock(bookDto.getStock());
        book.setPath_img(bookDto.getPath_img());
        book.setLanguage(bookDto.getLanguage());
        book.setAuthors(bookDto.getAuthors());
        book.setCategories(bookDto.getCategories());
        		
        
        bookService.save(book);
        
        
        
        
        return new ResponseEntity(new Mensaje("libro actualizado"), HttpStatus.OK);
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Elimina un libro por isbn, necesario permisos de administrador")
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{isbn}")
    public ResponseEntity<?> delete(@PathVariable("isbn")long isbn){
        if(!bookService.existsById(isbn))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        try {
        	bookService.delete(isbn);
        }catch(Exception ex) {
        	return new ResponseEntity(new Mensaje("el libro no se puede eliminar consulta con el tecnico."), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(new Mensaje("libro eliminado"), HttpStatus.OK);
    }
  
}
