package com.jrg.backendbookscase.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jrg.backendbookscase.dto.LanguageDto;
import com.jrg.backendbookscase.dto.Mensaje;
import com.jrg.backendbookscase.entity.Language;
import com.jrg.backendbookscase.service.LanguageService;

import io.swagger.annotations.ApiOperation;


/**
 * Controlador de la entidad lenguaje.
 * 
 * @author Javier Rodríguez García
 *
 */
@RestController
@RequestMapping("/language")
@CrossOrigin(origins = "*")
public class LanguageController {
	
	@Autowired
	LanguageService languageService;
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Muestra una lista de lenguajes")
	@GetMapping("/lista")
	public ResponseEntity<List<Language>> list(){
		List<Language> list = languageService.list();
		return new ResponseEntity(list, HttpStatus.OK);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Muestra una lista de lenguajes por id")
    @GetMapping("/detail/{id}")
    public ResponseEntity<Language> getById(@PathVariable("id") int id){
        if(!languageService.existsById(id))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        Language language = languageService.getOne(id).get();
        return new ResponseEntity(language, HttpStatus.OK);
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Muestra una lista de lenguajes por nombres de lenguaje")
    @GetMapping("/detaillanguage/{nameLanguage}")
    public ResponseEntity<Language> getDetailName(@PathVariable("nameLanguage") String nameLanguage){
        if(!languageService.existsByLanguage(nameLanguage))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        Language language = languageService.getByLanguage(nameLanguage).get();
        return new ResponseEntity(language, HttpStatus.OK);
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Crea un lengueaje, sólo lo puede crear el administrador")
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody LanguageDto languageDto){
        if(StringUtils.isBlank(languageDto.getLanguage()))
            return new ResponseEntity(new Mensaje("el nombre del idioma es obligatorio"), HttpStatus.BAD_REQUEST);
        if(languageService.existsByLanguage(languageDto.getLanguage()))
            return new ResponseEntity(new Mensaje("ese idioma ya existe"), HttpStatus.BAD_REQUEST);

        Language language = new Language(languageDto.getLanguage());
        languageService.save(language);
        return new ResponseEntity(new Mensaje("idioma creado"), HttpStatus.OK);
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Actualiza un lengueaje, sólo lo puede actualizar el administrador")
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable("id")int id, @RequestBody LanguageDto languageDto){
        if(!languageService.existsById(id))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        if(languageService.existsByLanguage(languageDto.getLanguage()) && languageService.getByLanguage(languageDto.getLanguage()).get().getId() != id)
            return new ResponseEntity(new Mensaje("ese idioma ya existe"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(languageDto.getLanguage()))
            return new ResponseEntity(new Mensaje("el idioma es obligatorio"), HttpStatus.BAD_REQUEST);

        Language language = languageService.getOne(id).get();
        language.setLanguage(languageDto.getLanguage());

        languageService.save(language);
        return new ResponseEntity(new Mensaje("idioma actualizado"), HttpStatus.OK);
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Elimina un lengueaje, sólo lo puede eliminar el administrador")
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id")int id){
        if(!languageService.existsById(id))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        languageService.delete(id);
        return new ResponseEntity(new Mensaje("idioma eliminado"), HttpStatus.OK);
    }

}
