package com.jrg.backendbookscase.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jrg.backendbookscase.dto.CategoryDto;
import com.jrg.backendbookscase.dto.Mensaje;
import com.jrg.backendbookscase.entity.Category;
import com.jrg.backendbookscase.service.CategoryService;

import io.swagger.annotations.ApiOperation;


/**
 * Controlador de la entidad Categoria.
 * 
 * @author Javier Rodríguez García
 *
 */
@RestController
@RequestMapping("/category")
@CrossOrigin(origins = "*")
public class CategoryController {
	
	@Autowired
	CategoryService categoryService;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Muestra una lista de categorias")
	@GetMapping("/lista")
	public ResponseEntity<List<Category>> list(){
		List<Category> list = categoryService.list();
		return new ResponseEntity(list, HttpStatus.OK);
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Muestra una categoria por id")
    @GetMapping("/detail/{id}")
    public ResponseEntity<Category> getById(@PathVariable("id") int id){
        if(!categoryService.existsById(id))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        Category category = categoryService.getOne(id).get();
        return new ResponseEntity(category, HttpStatus.OK);
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Muestra una categoria por el nombre")
    @GetMapping("/detailname/{name}")
    public ResponseEntity<Category> getDetailName(@PathVariable("name") String name){
        if(!categoryService.existsByName(name))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        Category category = categoryService.getByName(name).get();
        return new ResponseEntity(category, HttpStatus.OK);
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Crea una categoria, sólo la puede crear el administrador")
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody CategoryDto categoryDto){
        if(StringUtils.isBlank(categoryDto.getName()))
            return new ResponseEntity(new Mensaje("el nombre de la categoria es obligatorio"), HttpStatus.BAD_REQUEST);
        if(categoryService.existsByName(categoryDto.getName()))
            return new ResponseEntity(new Mensaje("ese nombre ya existe"), HttpStatus.BAD_REQUEST);

        Category category = new Category(categoryDto.getName());
        categoryService.save(category);
        return new ResponseEntity(new Mensaje("categoria creada"), HttpStatus.OK);
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Actualiza una categoria por id, sólo la puede crear el administrador")
    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@PathVariable("id")int id, @RequestBody CategoryDto categoryDto){
        if(!categoryService.existsById(id))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        if(categoryService.existsByName(categoryDto.getName()) && categoryService.getByName(categoryDto.getName()).get().getId() != id)
            return new ResponseEntity(new Mensaje("esa categoria ya existe"), HttpStatus.BAD_REQUEST);
        if(StringUtils.isBlank(categoryDto.getName()))
            return new ResponseEntity(new Mensaje("el nombre es obligatorio"), HttpStatus.BAD_REQUEST);

        Category category = categoryService.getOne(id).get();
        category.setName(categoryDto.getName());

        categoryService.save(category);
        return new ResponseEntity(new Mensaje("categoria actualizada"), HttpStatus.OK);
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Elimina una categoria por id, sólo la puede crear el administrador")
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id")int id){
        if(!categoryService.existsById(id))
            return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
        try {
           categoryService.delete(id);
        }catch(Exception ex) {
        	return new ResponseEntity(new Mensaje("la categoria no se puede eliminar porque está en uso"), HttpStatus.BAD_REQUEST);
        }   
        return new ResponseEntity(new Mensaje("categoria eliminada"), HttpStatus.OK);
    }

}
