package com.jrg.backendbookscase.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jrg.backendbookscase.dto.SalesDto;
import com.jrg.backendbookscase.dto.Mensaje;
import com.jrg.backendbookscase.entity.Sales;
import com.jrg.backendbookscase.service.SalesService;

import io.swagger.annotations.ApiOperation;


/**
 * Controlador de la entidad venta.
 * 
 * @author Javier Rodríguez García
 *
 */
@RestController
@RequestMapping("/sales")
@CrossOrigin(origins = "*")
public class SalesController {
	
	@Autowired
	SalesService salesService;
	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Muestra una lista de ventas")
	@GetMapping("/lista")
	public ResponseEntity<List<Sales>> list(){
		List<Sales> list = salesService.list();
		return new ResponseEntity(list, HttpStatus.OK);
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Muestra una lista de ventas por id")
    @GetMapping("/detail/{id}")
    public ResponseEntity<Sales> getById(@PathVariable("id") int id){
    	if(!salesService.existsById(id))
    		return new ResponseEntity(new Mensaje("no existe"), HttpStatus.NOT_FOUND);
    	
    	Sales sales = salesService.getOne(id).get();  
        return new ResponseEntity(sales, HttpStatus.OK);
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ApiOperation("Crea una lista de ventas por id, sólo lo puden crear el cliente y el administrador.")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody SalesDto salesDto){
    	
    	Sales sale = new Sales();
    	
    	sale.setSalesDate(salesDto.getSalesDate());
    	sale.setUser(salesDto.getUser());
    	
    	Sales saleSave = salesService.save(sale);

        return new ResponseEntity(saleSave, HttpStatus.OK);
    }
    


}
