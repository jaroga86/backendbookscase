-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: db_bookcase
-- ------------------------------------------------------
-- Server version	5.7.32-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authors`
--

DROP TABLE IF EXISTS `authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `surnames` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authors`
--

LOCK TABLES `authors` WRITE;
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` VALUES (10,'ANDREA ','WULF'),(11,'CHRISTOPHE','GALFARD'),(12,'PABLO ','ALCALDE SAN MIGUEL'),(13,'GABRIEL ','TORTELLAS'),(14,'ANGEL ','VIÑAS'),(18,'YUVAL','NOAH HARARI'),(19,'J.K. ','ROWLING '),(20,'PAUL ','KRUGMAN'),(21,'LUIS','DE GUINDOS'),(22,'ROBERT ','T. KIYOSAKI'),(24,'HARUKI','MURAKAMI'),(25,'STANISLAW','LEM'),(26,'Gabriel','Tortella'),(29,'STANISLAW','LEM'),(31,'Carmen','Mola'),(37,'Paloma','Sanchez'),(39,'Bruce','Eckel');
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authors_books`
--

DROP TABLE IF EXISTS `authors_books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authors_books` (
  `id_authors` int(11) NOT NULL,
  `id_isbn` bigint(20) NOT NULL,
  PRIMARY KEY (`id_authors`,`id_isbn`),
  KEY `fk_authors_books_books1_idx` (`id_isbn`),
  CONSTRAINT `fk_authors_books_authors1` FOREIGN KEY (`id_authors`) REFERENCES `authors` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_authors_books_books1` FOREIGN KEY (`id_isbn`) REFERENCES `books` (`isbn`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authors_books`
--

LOCK TABLES `authors_books` WRITE;
/*!40000 ALTER TABLE `authors_books` DISABLE KEYS */;
INSERT INTO `authors_books` VALUES (39,9780131872486),(31,9788408249849),(19,9788416367757),(25,9788418668197),(31,9788420433189),(12,9788428398770),(20,9788429126464),(10,9788430618088),(22,9788466332125),(13,9788494445583),(19,9788498382679),(19,9788498382693),(14,9788498928853),(21,9788499425375),(18,9788499926711);
/*!40000 ALTER TABLE `authors_books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `books` (
  `isbn` bigint(20) NOT NULL,
  `name` varchar(45) NOT NULL,
  `n_pag` int(11) NOT NULL,
  `summary` varchar(3000) NOT NULL,
  `binding` varchar(45) NOT NULL,
  `price` double NOT NULL,
  `stock` int(11) NOT NULL,
  `path_img` varchar(45) NOT NULL,
  `id_languages` int(11) NOT NULL,
  PRIMARY KEY (`isbn`),
  UNIQUE KEY `isbn_UNIQUE` (`isbn`),
  KEY `fk_books_languages1_idx` (`id_languages`),
  CONSTRAINT `fk_books_languages1` FOREIGN KEY (`id_languages`) REFERENCES `languages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES (9780131872486,'Thinking in Java',1150,'Thinking in Java should be read cover to cover by every Java programmer, then kept close at hand for frequent reference. The exercises are challenging, and the chapter on Collections is superb! Not only did this book help me to pass the Sun Certified Java Programmer exam; it’s also the first book I turn to whenever I have a Java question.”\n―Jim Pleger, Loudoun County (Virginia) Government\n\n“ Much better than any other Java book I’ve seen. Make that ‘by an order of magnitude’.... Very complete, with excellent right-to-the-point examples and intelligent, not dumbed-down, explanations.... In contrast to many other Java books I found it to be unusually mature, consistent, intellectually honest, well-written, and precise. IMHO, an ideal book for studying Java.”\n―Anatoly Vorobey, Technion University, Haifa, Israel\n\n“Absolutely one of the best programming tutorials I’ve seen for any language.”\n―Joakim Ziegler, FIX sysop\n\n“Thank you again for your awesome book. I was really floundering (being a non-C programmer), but your book has brought me up to speed as fast as I could read it. It’s really cool to be able to understand the underlying principles and concepts from the start, rather than having to try to build that conceptual model through trial and error. Hopefully I will be able to attend your seminar in the not-too-distant future.”\n―Randall R. Hawley, automation technician, Eli Lilly & Co.\n\n“This is one of the best books I’ve read about a programming language.... The best book ever written on Java.”\n―Ravindra Pai, Oracle Corporation, SUNOS product line\n\n“Bruce, your book is wonderful! Your explanations are clear and direct. Through your fantastic book I have gained a tremendous amount of Java knowledge. The exercises are also fantastic and do an excellent job reinforcing the ideas explained throughout the chapters. I look forward to reading more books written by you. Thank you for the tremendous service that you are providing by writing such great books. My code will be much better after reading Thinking in Java. I thank you and I’m sure any programmers who will have to maintain my code are also grateful to you.”\n―Yvonne Watkins, Java artisan, Discover Technologies, Inc.\n\n“Other books cover the what of Java (describing the syntax and the libraries) or the how of Java (practical programming examples). Thinking in Java is the only book I know that explains the why of Java: Why it was designed the way it was, why it works the way it does, why it sometimes doesn’t work, why it’s better than C++, why it’s not. Although it also does a good job of teaching the what and how of the language, Thinking in Java is definitely the thinking person’s choice in a Java book.','EPUB',45.11,200,'61-ZAFCWcLL._AC_SY780_.jpg',2),(9788408249849,'La bestia',544,'De manera magistral, Carmen Mola teje, con los hilos del mejor thriller, una novela frenética e implacable\nCorre el año 1834 y Madrid, una pequeña ciudad que trata de abrirse paso más allá de las murallas que la rodean, sufre una terrible epidemia de cólera. Pero la peste no es lo único que aterroriza a sus habitantes: en los arrabales aparecen cadáveres desmembrados de niñas que nadie reclama. Todos los rumores apuntan a la Bestia, un ser a quien nadie ha visto pero al que todos temen.\n\nCuando la pequeña Clara desaparece, su hermana Lucía, junto con Donoso, un policía tuerto, y Diego, un periodista buscavidas, inician una frenética cuenta atrás para encontrar a la niña con vida. En su camino tropiezan con fray Braulio, un monje guerrillero, y con un misterioso anillo de oro con dos mazas cruzadas que todo el mundo codicia y por el que algunos están dispuestos a matar.','Tapa dura',21.75,100,'9788408249849.jpg',1),(9788416367757,'HARRY POTTER I EL LLEGAT MALEÏT',480,'Basada en una nova història original de J.K. Rowling, John Tiffany i Jack Thorne, una nova obra de teatre de Jack Thorne, Harry Potter i el llegat maleït és la vuitena història en la sèrie de Harry Potter i la primera història oficial de Harry Potter que s\'ha portat al teatre. L\' estrena mundial de l\'obra tindrà lloc al West End de Londres el 30 de juliol de 2016. Ser Harry Potter no ha sigut mai senzill, i continua sense ser-ho ara que és un empleat carregat de feina a la Conselleria d?Afers Màgics, marit i pare de tres fills en edat escolar. Mentre el Harry ha de conviure amb un passat que es resisteix a quedar enrere, el seu fill petit, l?Albus, intenta alliberar-se del pes d?un llegat familiar que no ha volgut mai. Però de cop el passat i el present es conjuguen de manera sinistra, i pare i fill descobreixen una veritat desagradable: a vegades el mal sorgeix als llocs més inesperats.','Tapa dura',18.05,25,'9788416367757.jpg',6),(9788418668197,'EL PROFESOR A. DONDA',96,'¡El holocausto informativo ha caído sobre el planeta! Ijon Tichy se acurruca delante de una cueva de la que le ha expulsado un gorila, mientras graba sus memorias en tablillas de arcilla, como hacían los babilonios. Su colega, el profesor Affidavit Donda, ha hecho un descubrimiento asombroso: ¡la información almacenada en las computadoras tiene un peso medible! Todos los ordenadores del mundo se han destruido a sí mismos y ello ha provocado la desaparición de la totalidad de los bancos de datos informatizados que hay en la Tierra. Aunque esto ha causado el colapso del Primer Mundo, ha supuesto un enorme alivio para el Tercero. No solo las armas modernas se han quedado obsoletas, sino que el sistema monetario mundial también ha sido abolido de un plumazo, lo que ha obligado a la humanidad a regresar a una suerte de paraíso perdido (que de paradisiaco tiene más bien poco).','Tapa dura',13.54,15,'9788418668197.jpg',1),(9788420433189,'La novia gitana',408,'En Madrid se mata poco», le decía al joven subinspector Ángel Zárate su mentor en la policía; «pero cuando se mata, no tiene nada que envidiarle a ninguna ciudad del mundo», podría añadir la inspectora Elena Blanco, jefa de la Brigada de Análisis de Casos, un departamento creado para resolver los crímenes más complicados y abyectos.\n\nSusana Macaya, de padre gitano pero educada como paya, desaparece tras su fiesta de despedida de soltera. El cadáver es encontrado dos días después en la Quinta de Vista Alegre del madrileño barrio de Carabanchel. Podría tratarse de un asesinato más, si no fuera por el hecho de que la víctima ha sido torturada siguiendo un ritual insólito y atroz, y de que su hermana Lara sufrió idéntica suerte siete años atrás, también en vísperas de su boda. El asesino de Lara cumple condena desde entonces, por lo que solo caben dos posibilidades: o alguien ha imitado sus métodos para matar a la hermana pequeña, o hay un inocente encarcelado.\n\nPor eso el comisario Rentero ha decidido apartar a Zárate del caso y encargárselo a la veterana Blanco, una mujer peculiar y solitaria, amante de la grappa, el karaoke, los coches de coleccionista y las relaciones sexuales en todoterrenos. Una policía vulnerable, que se mantiene en el cuerpo para no olvidar que en su vida existe un caso pendiente, que no ha podido cerrar.\n\nInvestigar a una persona implica conocerla, descubrir sus secretos y contradicciones, su historia. En el caso de Lara y Susana, Elena Blanco debe asomarse a la vida de unos gitanos que han renunciado a sus costumbres para integrarse en la sociedad y a la de otros que no se lo perdonan, y levantar cada velo para descubrir quién pudo vengarse con tanta saña de ambas novias gitanas.','Tapa dura',9.45,200,'9788420462653.jpg',1),(9788428398770,'ELECTROTECNIA',424,'Esta obra desarrolla los contenidos del módulo profesional de Electrotecnia que siguen los alumnos del Ciclo Formativo de grado medio de Instalaciones Eléctricas y Automáticas, perteneciente a la familia profesional de Electricidad y Electrónica. Además, puede ser de utilidad para todos aquellos profesionales del sector eléctrico que deseen reforzar sus conocimientos sobre la materia.  Se ha preparado con el objetivo principal de que los contenidos resulten claros y asequibles a la vez que didácticos y prácticos, sin descuidar por ello el rigor científico. Asimismo, estamos seguros de que los temas aquí tratados serán de gran ayuda para comprender los fundamentos de la electrotecnia. Para ello se han elaborado 21 Unidades didácticas que combinan la teoría con multitud de ejemplos y de actividades experimentales con montajes prácticos. De igual modo, en todas ellas se desarrollan numerosas actividades resueltas que hacen mucho más fácil la comprensión de las explicaciones teóricas.  En resumen, se trata de una obra esencialmente práctica tanto para los estudiantes del módulo profesional de Electrotecnia como para profesionales y aficionados a esta materia.','Tapa blanda',31.83,690,'9788428398770.jpg',1),(9788429126464,'FUNDAMENTOS DE ECONOMIA ',120,'Cada vez son más los profesores en todo el mundo que están introduciendo a sus alumnos en los principios fundamentales de la economía a través de las obras del premio Nobel Paul Krugman.  En la tercera edición del libro FUNDAMENTOS DE ECONOMÍA, hemos aprendido que siempre hay espacio para mejorar. Por lo tanto, en esta edición se ha hecho una revisión con tres objetivos fundamentales: ampliar el atractivo del libro pensando en los estudiantes de ciencias empresariales, actualizar sus contenidos para estar al día en los temas que se tratan a lo largo de sus capítulos y hacer que la obra resulte todavía más accesible al lector, consiguiendo una mejor experiencia educativa en la enseñanza de la economía.','Tapa dura',44,20,'9788429126464.jpg',1),(9788430618088,'LA INVENCION DE LA NATURALEZA',584,'La invención de la naturalezarevela la extraordinaria vida del visionario naturalista alemán Alexander von Humboldt (1769-1859) y cómo creó una nueva forma de entender la naturaleza. Humboldt fue un intrépido explorador y el científico más famoso de su época. Su agitada vida estuvo repleta de aventuras y descubrimientos: escaló los volcanes más altos del mundo, remó por el Orinoco y recorrió una Siberia infestada de ántrax. Capaz de percibir la naturaleza como una fuerza global interconectada, Humboldt descubrió similitudes entre distintas zonas climáticas de todo el mundo, y previó el peligro de un cambio climático provocado por el hombre. Convirtió la observación científica en narrativa poética, y sus escritos inspiraron no solo a naturalistas y escritores como Darwin, Wordsworth y Goethe, sino también a políticos como Jefferson o Simón Bolívar. Además, fueron las ideas de Humboldt las que llevaron a John Muir a perseverar en sus teorías, y a Thoreau a escribir suWalden. Wulf rastrea la influencia de Humboldt en las grandes mentes de su tiempo, a las que inspiró en ámbitos como la revolución, la teoría de evolución, la ecología,...','Tapa dura',23.9,50,'9788430618088.jpg',1),(9788466332125,'PADRE RICO, PADRE POBRE',264,'Padre rico, padre pobrees el libro de finanzas personales nº 1 en todo el mundo, el manual de Robert T. Kiyosaki que enseña a las personas a hacerse millonarias. BestsellerdeThe New York Times, delWall Street Journal, de Business Week y deUSA Today. Padre rico, padre pobrete ayudará a... ...derribar el mito de que necesitas tener ingresos elevados para hacerte rico. ...desafiar la creencia de que tu casa es una inversión. ...demostrar a los padres por qué no deben confiar en el sistema escolar para que sus hijos aprendan a manejar el dinero. ...definir de una vez y para siempre qué es una inversión, y qué es una obligación. ...saber qué debes enseñar a tus hijos sobre el dinero para que tengan éxito financiero en el futuro. Robert T. Kiyosaki ha transformado radicalmente la forma en que millones de personas alrededor del mundo perciben el concepto del dinero. Con perspectivas que contradicen el conocimiento convencional, Robert, también conocido como el «maestro» millonario, se ha ganado una gran reputación por hablar claro, ser irreverente y tener valor. Es reconocido en todo el mundo como un defensor apasionado de la ...','Tapa blanda',8.95,25,'9788466332125.jpg',1),(9788494445583,'CATALUÑA EN ESPAÑA: HISTORIA Y MITO ',540,'Cataluña en España. Historia y mito sintetiza la historia de Cataluña y la de España en su recorrido común desde la Edad Media hasta los hechos más recientes. Si la Historia está, y debe estar, siempre de actualidad, el tema de la relación entre Cataluña y el resto de España justifica plenamente esta obra, que profundiza en todos los aspectos relevantes de una cuestión a menudo analizada superficialmente. Con gran rigor histórico, el libro ofrece un panorama inusual por la riqueza de su documentación, y discute algunas de las versiones sesgadas e interesadas de la Historia que han abundado en los últimos tiempos. Gabriel Tortella y los coautores del libro José Luis García Ruiz, Clara Eugenia Núñez y Gloria Quiroga- ponen aquí su amplia trayectoria y su prestigio académico al servicio de la Historia como herramienta que nos ayuda a entender el pasado y a gestionar el presente. Historia y presente, pues la obra no omite ninguno de los temas más candentes -desde las deudas históricas hasta las balanzas comerciales y fiscales; desde las políticas identitarias llevadas a cabo por la Generalitat de Catalunya hasta su reciente giro hacia la ruptura de uno de los países más antiguos del mundo-, aportando además elementos novedosos de investigación, como la cuantificación de una hipotética deuda histórica que pudie...','Tapa blanda',21.5,25,'9788494445583.jpg',1),(9788498382679,'HARRY POTTER Y LA CAMARA SECRETA',288,'Tras derrotar una vez más a lord Voldemort, su siniestro enemigo en Harry Potter y la piedra filosofal, Harry espera impaciente en casa de sus insoportables tíos el inicio del segundo curso del Colegio Hogwarts de Magia. Sin embargo, la espera dura poco, pues un elfo aparece en su habitación y le advierte que una amenaza mortal se cierne sobre la escuela. Así pues, Harry no se lo piensa dos veces y, acompañado de Ron, su mejor amigo, se dirige a Hogwarts en un coche volador. Pero ¿puede un aprendiz de mago defender la escuela de los malvados que pretenden destruirla? Sin saber que alguien ha abierto la Cámara de los Secretos, dejando escapar una serie de monstruos peligrosos, Harry y sus amigos Ron y Hermione tendrán que enfrentarse con arañas gigantes, serpientes encantadas, fantasmas enfurecidos y, sobre todo, con la mismísima reencarnación de su más temible adversario.','Tapa blanda',7.13,20,'9788498382679.jpg',1),(9788498382693,'QUIDDITCH A TRAVES DE LOS TIEMPOS',78,'Si alguna vez te has preguntado de dónde proviene la snitch dorada, cómo adquieren vida las bludgers o por qué los Wigtown Wanderers llevan un cuchillo de carnicero dibujado en el uniforme, entonces querrás leer Quidditch a través de los tiempos. Esta edición es una copia del ejemplar que está en la biblioteca del Colegio Hogwarts y que los jóvenes fanáticos del quidditch consultan casi a diario. Los beneficios de la venta de este libro se destinarán a Comic Relief, que utilizará tu dinero para continuar salvando y mejorando vidas, un trabajo que es aún más importante y sorprendente que los tres segundos y medio que tardó Roderick Plumpton en capturar la snitch dorada en 1921.','Tapa blanda',4.75,26,'9788498382693.jpg',1),(9788498928853,'AÑADIR LA OTRA CARA DEL CAUDILLO',100,'Las biografías de Francisco Franco siguen difundiendo mitos que nos ocultan la realidad del personaje y de su actuación. Basándose en nueva documentación, Ángel Viñas destroza algunos de los mitos que sigue difundiendo un pretendido «revisionismo» histórico y nos ofrece nuevas perspectivas sobre temas tan fundamentales como la naturaleza real del poder dictatorial -asentado no sólo en las leyes publicadas, sino también en los «decretos reservados»-, sobre la base militar en que se apoyó el modelo de disuasión del régimen, sobre la querencia nazi de Franco y la gravedad de su compromiso con Hitler o sobre un tema tan vital, y tan ignorado, como el oscuro origen de la fortuna del Caudillo. A diferencia de lo que ocurre con buena parte de la historiografía «neofranquista», en las páginas de este libro cada afirmación está apoyada por la correspondiente documentación.','EPUB',8,30,'9788498928853.jpg',1),(9788499425375,'ESPAÑA AMENAZADA',192,'Cuenta en primera persona cómo España evitó la claudicación del rescate En el año 2012 se desencadenó en España una crisis económica sin precedentes. Nunca antes la economía española había quedado tan aprisionada y por ello, nunca antes había sufrido una recesión tan larga y profunda. Y no solo en lo económico, también en lo moral, la crisis sacó a la luz comportamientos indeseables. En ese clima de desconfianza—nadie prestaba un euro, nadie se fiaba—todo empujaba a que España pidiera el rescate a los socios europeos. Pero esa decisión habría supuesto la pérdida de autonomía en la política económica y que nuestra autoestima como país se viera profundamente dañada. Luis de Guindos cuenta en primera persona cómo España evitó la claudicación del rescate. Las reuniones con los socios internacionales, las presiones internas, los momentos críticos…todo ello forma parte de la historia de lo vivido en un año excepcional en el que el proyecto del euro estuvo seriamente amenazado. Como ministro de Economía del Gobierno de Mariano Rajoy, De Guindos ha sido protagonista en primera fila de todo lo acontecido. Desde las primeras medidas para cortar la sangría del déficit, la defensa de las reformas a los socios europeos y a los inversores internacionales, la crisis de Bankia y sus derivadas, el programa de asistencia fin...','Tapa blanda',17.95,250,'9788499425375.jpg',1),(9788499926711,'HOMO DEUS: BREVE HISTORIA DEL MAÑANA',496,'Tras el éxito deSapiens. De animales a dioses, Yuval Noah Harari vuelve su mirada al futuro para ver hacia dónde nos dirigimos. La guerra es algo obsoleto.Es más probable quitarse la vida que morir en un conflicto bélico. La hambruna está desapareciendo.Es más habitual sufrir obesidad que pasar hambre. La muerte es solo un problema técnico.Adiós igualdad. Hola inmortalidad. ¿Qué nos depara el futuro? Yuval Noah Harari, autorbestsellerdeSapiens. De animales a dioses, augura un mundo no tan lejano en el cual nos veremos enfrentados a una nueva serie de retos.Homo Deusexplora los proyectos, los sueños y las pesadillas que irán moldeando el siglo XXI -desde superar la muerte hasta la creación de la inteligencia artificial. -Cuando tu Smartphone te conozca mejor de lo que te conoces a ti mismo, ¿seguirás escogiendo tu trabajo, a tu pareja y a tu presidente? -Cuando la inteligencia artificial nos desmarque del mercado laboral, ¿encontrarán los millones de desempleados algún tipo de significado en las drogas o los juegos virtuales? -Cuando los cuerpos y cerebros sean productos de diseño, ¿cederá la selección natural el paso al diseño inteligente?  Esto es el futuro de la evolución. Esto es Homo Deus.','Tapa dura',23.9,25,'9788499926711.jpg',1);
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Ciencias'),(2,'Historia'),(3,'Guías de viajes'),(4,'Economía'),(7,'Juvenil'),(9,'Psicología'),(10,'Informática'),(11,'Poesía');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories_books`
--

DROP TABLE IF EXISTS `categories_books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories_books` (
  `id_categories` int(11) NOT NULL,
  `id_isbn` bigint(20) NOT NULL,
  PRIMARY KEY (`id_categories`,`id_isbn`),
  KEY `fk_categories_books_books1_idx` (`id_isbn`),
  CONSTRAINT `fk_categories_books_books1` FOREIGN KEY (`id_isbn`) REFERENCES `books` (`isbn`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_categories_books_categories1` FOREIGN KEY (`id_categories`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories_books`
--

LOCK TABLES `categories_books` WRITE;
/*!40000 ALTER TABLE `categories_books` DISABLE KEYS */;
INSERT INTO `categories_books` VALUES (10,9780131872486),(1,9788408249849),(7,9788416367757),(1,9788418668197),(1,9788420433189),(1,9788428398770),(4,9788429126464),(1,9788430618088),(4,9788466332125),(2,9788494445583),(7,9788498382679),(7,9788498382693),(2,9788498928853),(4,9788499425375),(2,9788499926711);
/*!40000 ALTER TABLE `categories_books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detailsale`
--

DROP TABLE IF EXISTS `detailsale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `detailsale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL,
  `id_isbn` bigint(20) NOT NULL,
  `id_sales` int(11) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_detailsale_books1_idx` (`id_isbn`),
  KEY `fk_detailsale_sales1_idx` (`id_sales`),
  CONSTRAINT `fk_detailsale_books1` FOREIGN KEY (`id_isbn`) REFERENCES `books` (`isbn`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detailsale_sales1` FOREIGN KEY (`id_sales`) REFERENCES `sales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detailsale`
--

LOCK TABLES `detailsale` WRITE;
/*!40000 ALTER TABLE `detailsale` DISABLE KEYS */;
/*!40000 ALTER TABLE `detailsale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (3),(3),(3),(3);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'Español'),(2,'Inglés'),(3,'Frances'),(4,'Chino'),(5,'Griego'),(6,'Catalan');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_USER');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales`
--

DROP TABLE IF EXISTS `sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `id_users` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sales_users1_idx` (`id_users`),
  CONSTRAINT `fk_sales_users1` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales`
--

LOCK TABLES `sales` WRITE;
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `surnames` varchar(45) NOT NULL,
  `password` varchar(250) NOT NULL,
  `email` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `cp` int(11) NOT NULL,
  `location` varchar(45) NOT NULL,
  `province` varchar(45) NOT NULL,
  `state` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin','$2a$10$apdDdz73bUPN24XE.OZNneZJqbSpF5fNRcG7Y8vMM9m0uHkex0LVi','admin@bookscase.com','C/Povedilla, 12',28009,'Madrid','Madrid',1),(52,'user1','user1 user1','$2a$10$4gXu7kCMVtriHhRLaawEyu3QwnjPwPhWAy9eWF4MGi6FHx5BwPnnm','user1@test.com','calle de las delicias',10300,'Navalmoral de la Mata','Cáceres',1),(53,'user2','user2 user2','$2a$10$6q9fgtNvO7d0CF3IrExt3esIGpnkkhdF.JXexQ13x0pxl66q8yP9.','user2@test.com','Paseo de la diputación',10680,'Malpartida de Plasencia','Cáceres',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_roles`
--

DROP TABLE IF EXISTS `users_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_roles` (
  `id_users` int(11) NOT NULL,
  `id_roles` int(11) NOT NULL,
  PRIMARY KEY (`id_users`,`id_roles`),
  KEY `fk_users_roles_roles1_idx` (`id_roles`),
  CONSTRAINT `FKq0tw71wffd6bfr4fofq7iiqqi` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_users_roles_roles1` FOREIGN KEY (`id_roles`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_roles`
--

LOCK TABLES `users_roles` WRITE;
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;
INSERT INTO `users_roles` VALUES (1,1),(52,2),(53,2);
/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-08  7:59:14
